exports = typeof window === 'undefined' ? global : window;

exports.arraysAnswers = {
  indexOf: function (arr, item) {
		return arr.indexOf(item);
  },

  sum: function (arr) {
		return arr.reduce(function (a, b) {
			return a + b;
		}, 0);
  },

  remove: function (arr, item) {
		var index = arr.indexOf(item);
		while (index !== -1) {
			arr.splice(index, 1);
			index = arr.indexOf(item);
		}
		return arr;
  },

  removeWithoutCopy: function (arr, item) {
		var index = arr.indexOf(item);
		while (index !== -1) {
			arr.splice(index, 1);
			index = arr.indexOf(item);
		}
		return arr;
  },

  append: function (arr, item) {
		arr.push(item);
		return arr;
  },

  truncate: function (arr) {
		arr.pop();
		return arr;
  },

  prepend: function (arr, item) {
		arr.unshift(item);
		return arr;
  },

  curtail: function (arr) {
		arr.shift();
		return arr;
  },

  concat: function (arr1, arr2) {
		return arr1.concat(arr2);
  },

  insert: function (arr, item, index) {
		arr.splice(index, 0, item);
		return arr;
  },

  count: function (arr, item) {
		return arr.filter(function (i) {
			return i === item;
		}).length;
  },

  duplicates: function (arr) {
		var arr_sorted = arr.slice().sort();
		var dupes = [];
		for (var i = 0; i < arr.length; i++) {
			if (arr_sorted[i + 1] === arr_sorted[i]) {
				if (arr_sorted[i] !== dupes[dupes.length - 1]) dupes.push(arr_sorted[i]);
			}
		}
		return dupes;
  },

  square: function (arr) {
		return arr.map(function (i) {
			return i * i;
		});
  },

  findAllOccurrences: function (arr, target) {
		var occurances = [];
		for (var i = 0; i < arr.length; i++) {
			if (arr[i] === target) occurances.push(i);
		}

		return occurances;
  }
};
