exports = typeof window === 'undefined' ? global : window;

exports.asyncAnswers = {

	async: function (value) {
		const executor = function (resolve, reject) {
			resolve(value);
		};
		return new Promise(executor);
	},

	manipulateRemoteData: function (url) {
		function executor(resolve, reject) {
			var request = new XMLHttpRequest();
			request.onreadystatechange = function () {
				if (request.readyState == 4 && request.status == 200) {
					var data = JSON.parse(request.responseText);

					var names = data.people.map(function (person) {
						return person.name;
					});

					resolve(names.sort());
				}
			};

			request.open("GET", url, true);
			request.send();
		}

		return new Promise(executor);
	}
};
