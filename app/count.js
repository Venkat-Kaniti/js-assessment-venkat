exports = typeof window === 'undefined' ? global : window;

exports.countAnswers = {
  count: function (start, end) {

    var cancelId;

		(function count() {
      if (start <= end) {
				console.log(start++);
        cancelId = setTimeout(count, 100);
      }
    })();

		return {
			cancel: function () {
				clearTimeout(cancelId);
			}
		}
  }
};
